#!/bin/bash
#crontab
#* * * * * ~/sisop/modul_1/soal_4/minute_log.sh
#membuat file.log
#Mendapatkan waktu
waktu=$(date +"%Y%m%d%H%M%S")

#Mendapatkan ram dengan menggunakan perintah free -m
#NR==2 untuk memori, NR==3 untuk swap
ram=$(free -m | awk 'NR==2{print $2","$3","$4","$5","$6","$7}')
swap=$(free -m | awk 'NR==3{print $2","$3","$4}')

#Menentukan direktori target
path=~/

#Mendapatkan disk dengan menggunakan perintah du -sh
disk=$(du -sh "${path}" | awk '{print $1}')

#Menyimpan ram, path, disk ke dalam file log
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/"sisop/modul_1/soal_4/log/metrics_${waktu}.log"
echo "${ram},${swap},${path},${disk}" >> ~/"sisop/modul_1/soal_4/log/metrics_${waktu}.log"
chmod 600 /home/steven/sisop/modul_1/soal_4/log/metrics_${waktu}.log
