#!/bin/bash
#crontab
#0 * * * * /home/steven/sisop/modul_1/soal_4/aggregate_minutes_to_hourly_log.sh
#mendapatkan waktu saat ini
waktu=$(date +"%Y%m%d%H")
#declarasi nilai awal
count=0
path=~/
mkdir ~/sisop/modul_1/soal_4/log/combined_log/

#menghitung semua file log per menit yang sesuai dengan format waktu ke dalam satu file
for file in ~/sisop/modul_1/soal_4/log/*.log; do
    hour="${file:17:2}"
    cat "$file" >> ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log"
done
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
# Calculate max
max_memtot=$(awk -F',' '{ if ($1 > max_memtot || NR == 1) max_memtot = $1 } END { print max_memtot }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_memus=$(awk -F',' '{ if ($2 > max_memus || NR == 1) max_memus = $2 } END { print max_memus }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_memfr=$(awk -F',' '{ if ($3 > max_memfr || NR == 1) max_memfr = $3 } END { print max_memfr }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_memsh=$(awk -F',' '{ if ($4 > max_memsh || NR == 1) max_memsh = $4 } END { print max_memsh }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_membf=$(awk -F',' '{ if ($5 > max_membf || NR == 1) max_membf = $5 } END { print max_membf }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_memav=$(awk -F',' '{ if ($6 > max_memav || NR == 1) max_memav = $6 } END { print max_memav }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_swaptot=$(awk -F',' '{ if ($7 > max_swaptot || NR == 1) max_swaptot = $7 } END { print max_swaptot }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_swapus=$(awk -F',' '{ if ($8 > max_swapus || NR == 1) max_swapus = $8 } END { print max_swapus }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_swapfr=$(awk -F',' '{ if ($9 > max_swapfr || NR == 1) max_swapfr = $9 } END { print max_swapfr }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")

#get all pathsz and separate alphabet-numeric
pathsz=$(awk -F',' '{print $NF}' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_pathsz=$(echo "$pathsz" | sed 's/\([0-9]\+\)\([A-Za-z]\+\)/\1 \2/' | sort -nr | head -n 1 | awk '{print $1}')

# Calculate min
min_memtot=$(awk -F',' '{ if ($1 < min_memtot || NR == 1) min_memtot = $1 } END { print min_memtot }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_memus=$(awk -F',' '{ if ($2 < min_memus || NR == 1) min_memus = $2 } END { print min_memus }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_memfr=$(awk -F',' '{ if ($3 < min_memfr || NR == 1) min_memfr = $3 } END { print min_memfr }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_memsh=$(awk -F',' '{ if ($4 < min_memsh || NR == 1) min_memsh = $4 } END { print min_memsh }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_membf=$(awk -F',' '{ if ($5 < min_membf || NR == 1) min_membf = $5 } END { print min_membf }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_memav=$(awk -F',' '{ if ($6 < min_memav || NR == 1) min_memav = $6 } END { print min_memav }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_swaptot=$(awk -F',' '{ if ($7 < min_swaptot || NR == 1) min_swaptot = $7 } END { print min_swaptot }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_swapus=$(awk -F',' '{ if ($8 < min_swapus || NR == 1) min_swapus = $8 } END { print min_swapus }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_swapfr=$(awk -F',' '{ if ($9 < min_swapfr || NR == 1) min_swapfr = $9 } END { print min_swapfr }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_pathsz=$(echo "$pathsz" | sed 's/\([0-9]\+\)\([A-Za-z]\+\)/\1 \2/' | sort -n | head -n 1 | awk '{print $1}')

#AVG
avg_memtot=$(((max_memtot + min_memtot) / 2))
avg_memus=$(((max_memus + min_memus) / 2))
avg_memfr=$(((max_memfr + min_memfr) / 2))
avg_memsh=$(((max_memsh + min_memsh) / 2))
avg_membf=$(((max_membf + min_membf) / 2))
avg_memav=$(((max_memav + min_memav) / 2))
avg_swaptot=$(((max_swaptot + min_swaptot) / 2))
avg_swapus=$(((max_swapus + min_swapus) / 2))
avg_swapfr=$(((max_swapfr + min_swapfr) / 2))
avg_pathsz=$(((max_pathsz + min_pathsz) / 2))

#hasil
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
echo "minimum,$min_memtot,$min_memus,$min_memfr,$min_memsh,$min_membf,$min_memav,$min_swaptot,$min_swapus,$min_swapfr,$path,$min_pathsz""M" >> ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
echo "maximum,$max_memtot,$max_memus,$max_memfr,$max_memsh,$max_membf,$max_memav,$max_swaptot,$max_swapus,$max_swapfr,$path,$max_pathsz""M" >> ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
echo "average,$avg_memtot,$avg_memus,$avg_memfr,$avg_memsh,$avg_membf,$avg_memav,$avg_swaptot,$avg_swapus,$avg_swapfr,$path,$avg_pathsz""M" >> ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"

rm -r ~/"sisop/modul_1/soal_4/log/combined_log/

chmod 600 /home/steven/sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log
