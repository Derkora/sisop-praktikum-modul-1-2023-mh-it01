#!/bin/bash


#donlod
wget -O genshin --no-check-certificate -r 'https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2'


#unzip fileny
unzip "genshin" -d "genshin_folder"
cd genshin_folder
touch char_data.csv

#unzip foto genshin cari yae wangy (aku kecewa foto yae nya ga wangy)
unzip "genshin_character.zip" -d "genshin_char"
cd genshin_char

#decrypt namafile
for file in *; do
  if [ -f "$file" ]; then
    filename="${file%.*}"
    decoded_file=$(echo "$filename" | base64 -d)
    mv "$file" "$decoded_file"
  fi
done

#klompokin sesuai kampung dll aduh jadi kebanyakan tau lah
mkdir Mondstat Liyue Inazuma Sumeru Fontaine
regions=("Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine")
elements=("Pyro" "Hydro" "Electro" "Cryo" "Geo" "Anemo" "Dendro")
weapons=("Sword" "Claymore" "Polearm" "Catalyst" "Bow")
char_region=""
char_element=""
char_weapon=""
sword_usr=0
claymore_usr=0
polearm_usr=0
catalyst_usr=0
bow_usr=0

#cek csv buat cari char_data
for file in *; do
  if [ -f "$file" ]; then
    cd ../
    grep "$file" list_character.csv > char_data.csv
    #find region di char_data
    for region in "${regions[@]}"; do
	result=$(awk -v word="$region" '$0 ~ word {print}' char_data.csv)
	if [ -n "$result" ]; then
	  char_region="$region"
	  break
	fi
    done

    #find elemen di char_data
    for element in "${elements[@]}"; do
        result=$(awk -v word="$element" '$0 ~ word {print}' char_data.csv)
        if [ -n "$result" ]; then
          char_element="$element"
          break
        fi
    done

    #find weapon di char_data
    for weapon in "${weapons[@]}"; do
        result=$(awk -v word="$weapon" '$0 ~ word {print}' char_data.csv)
        if [ -n "$result" ]; then
          char_weapon="$weapon"
	  #sekalian ngitungin siapaaja yang make ygy
	  case "$char_weapon" in
	    "Sword")
		((sword_usr++))
	    ;;
	    "Claymore")
                ((claymore_usr++))
            ;;
	    "Polearm")
                ((polearm_usr++))
            ;;
	    "Catalyst")
                ((catalyst_usr++))
            ;;
	    "Bow")
                ((bow_usr++))
            ;;
	  esac
          break
        fi
    done

    #rename foto wangy sesuai format
    cd genshin_char
    mv "$file" ./"$char_region"
    cd "$char_region"
    mv "$file" "$file - $char_region - $char_element - $char_weapon"
    cd ../
  fi
done

#dump owo
cd ../ && rm list_character.csv char_data.csv genshin_character.zip
cd ../ && rm genshin

#disuru print huft
echo "Number of character(s) using Sword: $sword_usr"
echo "Number of character(s) using Claymore: $claymore_usr"
echo "Number of character(s) using Polearm: $polearm_usr"
echo "Number of character(s) using Catalyst: $catalyst_usr"
echo "Number of character(s) using Bow: $bow_usr"
