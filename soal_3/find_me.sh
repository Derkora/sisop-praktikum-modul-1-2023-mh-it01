#!/bin/bash

#list region
regions=("Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine")

#keluar masuk folder region
for region in "${regions[@]}"; do
cd ./genshin_folder/genshin_char/"$region"

  #extract foto2 wangy
  for file in *; do
    waktu=$(date +"%Y%m%d%H%M%S")
    img_path="./${file}.jpeg"
    if [ -f "$file" ]; then
	steghide extract -sf "$file" -p ""
	name=$(awk -F' -' '{print$1}' <<< "$file")
	cat "$name".txt | base64 -d > ~/sisop/modul_1/soal_3/extracted.txt
    fi

    status="NOT FOUND"
    url=$(cat ~/sisop/modul_1/soal_3/extracted.txt)
    url_regex='https?://\S+'
    if [ $url=~$url_regex ]; then
	#donlod
	wget "$url"
	status="FOUND"
    fi

    echo "[$waktu] [$status] [$img_path]" >> ~/sisop/modul_1/soal_3/image.log

    if [ $status="FOUND" ]; then
    	break
    fi
    rm "$name".txt

    sleep 60
  done
if [ $status="FOUND" ]; then
    	break
fi
cd ../../../
done

#pindain file eter lumin wangy ke depan + txt mami lisa
mv ~/sisop/modul_1/soal_3/genshin_folder/genshin_char/Mondstat/eab0659ba060b0624df5d28b4910b2fb.jpg ~/sisop/modul_1/soal_3/eab0659ba060b0624df5d28b4910b2fb.jpg
mv ~/sisop/modul_1/soal_3/genshin_folder/genshin_char/Mondstat/Lisa.txt ~/sisop/modul_1/soal_3/Lisa.txt
rm ~/sisop/modul_1/soal_3/extracted.txt
