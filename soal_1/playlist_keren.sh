#!/bin/bash
#soal 1.a
#awk '$4 = "hip hop" {print $0}' farreledgy.csv | sort -k15 -n | head -n 5
echo "top 5 lagu hip hop"
grep "hip hop" farreledgy.csv | sort -t',' -k15 -n -r | head -n 5
echo
#soal 1.b
echo "top 5 lagu John Mayer dengan popularity terendah"
grep "John Mayer" farreledgy.csv | sort -t',' -k15  | head -n 5
echo
#soal 1.c
echo "top 10 lagu terbaik di tahun 2004"
grep "2004" farreledgy.csv | sort -t',' -k15 -r -n | head -n 10
echo 
#soal 1.d
echo "top 5 lagu terbaik ibu Sri"
grep "Sri" farreledgy.csv | sort -t',' -k15 -r -n | tail -n 5
