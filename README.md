# sisop-praktikum-modul-1-2023-MH-IT01

## Anggota Kelompok

| NRP        | Nama                    |
|:----------:|:-----------------------:|
| 5027221021 | Steven Figo             |
| 5027221024 | Wilson Matthew Thendry  |
| 5027221048 | Muhammad Arsy Athallah  |

- [Peraturan](#peraturan) 
- [Soal](#soal)
- [Detail Tambahan](#detail-tambahan)
- [Penjelasan](#penjelasan)
  - [Soal 1](#soal-1)
  - [Soal 2](#soal-2)
  - [Soal 3](#soal-3)
  - [Soal 4](#soal-4)
- [Revisi](#revisi)

## Peraturan

1. Waktu pengerjaan dimulai Senin (18/9) setelah sesi lab hingga Sabtu (23/9) pukul 22.00 WIB.
2. Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal dalam bentuk Readme(gitlab).
3. Format nama repository gitlab “sisop-praktikum-modul-[Nomor Modul]-2023-[Kode Dosen Kelas]-[Nama Kelompok]” (contoh:sisop-praktikum-modul-1-2023-MH-IT01).
4. Struktur repository seperti berikut:
	- soal_1:
		- playlist_keren.sh
	- soal_2:
		- login.sh
		- register.sh
	- soal_3:
		- genshin.sh
		- find_me.sh
	- soal_4:
		- minute_log.sh
		- aggregate_minutes_to_hourly_log.sh
Jika melanggar struktur repo akan dianggap sama dengan curang dan menerima konsekuensi sama dengan melakukan kecurangan.
5. Setelah pengerjaan selesai, semua script bash, awk, dan file yang berisi cron job ditaruh di gitlab masing - masing kelompok, dan link gitlab diletakkan pada form yang disediakan. Pastikan gitlab di setting ke publik.
6. Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati maka akan dinilai berdasarkan commit terakhir.
7. Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.
8. Jika ditemukan soal yang tidak dapat diselesaikan, harap menuliskannya pada Readme beserta permasalahan yang ditemukan.
9. Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.
10. Jika ditemukan indikasi kecurangan dalam bentuk apapun di pengerjaan soal shift, maka nilai dianggap 0.
11. Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.
12. Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift
13. Jika terdapat revisi soal akan dituliskan pada halaman terakhir

## Soal
1. Farrel ingin membuat sebuah playlist yang sangat edgy di tahun ini. Farrel ingin playlistnya banyak disukai oleh orang-orang lain. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa data untuk membuat playlist terbaik di dunia. Untung saja Farrel menemukan file playlist.csv yang berisi top lagu pada spotify beserta genrenya.
	- Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu. Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.
	- Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.
	- Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi
	- Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut. 

2. Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi membuat program register dan login agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.
	- Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.
	- Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.
		- Password tersebut harus di encrypt menggunakan base64
		- Password yang dibuat harus lebih dari 8 karakter
		- Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
		- Password tidak boleh sama dengan username
		- Harus terdapat paling sedikit 1 angka 
		- Harus terdapat paling sedikit 1 simbol unik 
	- Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.
	- Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal
	- Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.
	- Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.
	- Ex: 
		- LOGIN SUCCESS - Welcome, [username]
		- LOGIN FAILED - email [email] not registered, please register first
	- Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register.
		- Format: [date] [type] [message]
		- Type: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED
	- Ex:
		- [23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully
		- [23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]

3. Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.
	- Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter
		- Format: Nama - Region - Elemen - Senjata.jpg
	- Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada
		- Format: [Nama Senjata] : [total]
	 Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip
	- Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.
	- Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar
		- Format: [date] [type] [image_path]
		- Ex: 
			- [23/09/11 17:57:51] [NOT FOUND] [image_path]
			- [23/09/11 17:57:52] [FOUND] [image_path]
	- Hasil akhir:
		- genshin_character
		- find_me.sh
		- genshin.sh
		- image.log
		- [filename].txt
		- [image].jpg

4. Defatra sangat tergila-gila dengan laptop legion nya. Suatu hari, laptop legion nya mendadak rusak 🙁 Tentu saja, Defatra adalah programer yang harus 24/7 ngoding tanpa menggunakan vscode di laptop legion nya.  Akhirnya, dia membawa laptop legion nya ke tukang servis untuk diperbaiki. Setelah selesai diperbaiki, ternyata biaya perbaikan sangat mahal sehingga dia harus menggunakan uang hasil slot nya untuk membayarnya. Menurut Kang Servis, masalahnya adalah pada laptop Defatra yang overload sehingga mengakibatkan crash pada laptop legion nya. Untuk mencegah masalah serupa di masa depan, Defatra meminta kamu untuk membuat program monitoring resource yang tersedia pada komputer.
Buatlah program monitoring resource pada laptop kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.
    -  Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20230131150000.log. 
	- Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit. 
	- Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg_{YmdH}.log 
	- Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file. 
	- Note:
		- Nama file untuk script per menit adalah minute_log.sh
		- Nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
		- Semua file log terletak di /home/{user}/log
		- Semua konfigurasi cron dapat ditaruh di file skrip .sh nya masing-masing dalam bentuk comment
- Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

- Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size 
minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M 
maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M 
average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62M


## Detail Tambahan
1. 19 September 2023 3:24 PM WIB - Penambahan keterangan untuk soal no 1:
    - semakin besar nilai popularity, semakin populer
	- untuk kolom top genre == genre
	- sort berdasarkan popularity
2. 20 September 2023 11:38 PM WIB - Detail tambahan untuk soal 3
	- wget dimasukkan ke dalam script bash (file zip tidak didownload manual)
	- Format untuk menampilkan jumlah senjata tidak harus [Nama Senajata] : [Total] yang penting ada nama senjata dan total-nya
	- Untuk passphrase pada steghide, bisa dikosongkan saja


## Penjelasan 

### Soal 1
playlist_keren.sh
```bash
#!/bin/bash
#soal 1.a
#awk '$4 = "hip hop" {print $0}' farreledgy.csv | sort -k15 -n | head -n 5
echo "top 5 lagu hip hop"
grep "hip hop" farreledgy.csv | sort -t',' -k15 -n -r | head -n 5
echo
#soal 1.b
echo "top 5 lagu John Mayer dengan popularity terendah"
grep "John Mayer" farreledgy.csv | sort -t',' -k15  | head -n 5
echo
#soal 1.c
echo "top 10 lagu terbaik di tahun 2004"
grep "2004" farreledgy.csv | sort -t',' -k15 -r -n | head -n 10
echo 
#soal 1.d
echo "top 5 lagu terbaik ibu Sri"
grep "Sri" farreledgy.csv | sort -t',' -k15 -r -n | tail -n 5
```
Pertama kita menggunakan mkdir pada terminal ubuntu untuk membuat sebuah folder dengan nama “soal_1”
Lalu menggunakan command “cd” untuk masuk ke folder yang sudah kita buat tadi
Download file “playlist.csv” dan pindahkan dari download atau copy file nya kedalam folder “soal_1”
Rename file yang sudah didownload tadi menjadi farreledgy.csv
cat file farreledgy.csv untuk melihat isinya
Didalam folder “soal_1” buat folder bernama playlist_keren.sh. Nah kita mengerjakan nya di dalam .sh tersebut.
Gunakan command “bash playlist keren.sh” untuk masuk ke .sh dan mulai mengerjakannya
Merefer ke soal 1.a yang diminta untul menampilkan top 5 lagu hip hop teratas berdasarkan popularity, gunakan command
```bash
“awk '$4 = "hip hop" {print $0}' farreledgy.csv | sort -k15 -n | head -n 5
echo "top 5 hip hop"
grep "hip hop" farreledgy.csv | sort -t',' -k15 -n -r | head -n 5
```

echo 
  awk ‘$4: menuju ke tabel  4 dimana berisi list Top genre
  -k15: Menuju ke tabel 15 dimana berisi list popularity 

Melanjutkan soal 1.a , soal 1.b diminta mendisplay 5 lagu paling rendah ciptaan john mayer berdasarkan popularity, gunakan command (dibawah nya echo soal 1.a dengan diberi enter 1 kali agar rapi saja) 
```bash
echo "Top 5 Lagu John Mayer Popularity Rendah"
grep "John Mayer" farreledgy.csv | sort -t',' -k15  | head -n 5
echo
```

Menuju ke soal 1.c yang diminta untuk mendisplay 10 lagu terbaik tahun 2004 dengan menggunakan command
```bash
echo "Top 10 Lagu 2004"
grep "2004" farreledgy.csv | sort -t',' -k15 -r -n | head -n 10
echo
```

Terakhir soal 1.d diminta untuk mendisplay lagu paling edgy di dunia dengan menggunakan command
```bash
echo "Lagu Paling Edgy By Ibu Sri"
grep "Sri" farreledgy.csv | sort -t',' -k15 -r -n | tail -n 5
```

Setelah melakukan semua itu akan muncul list playlist seperti ini

tampilan playlist:
![image](/uploads/e78d9c0b0d8583662365ddc361a1922d/image.png)

### Soal 2
register.sh
```bash
#!/bin/bash
#waktu sekarang
waktu=$(date +"%d/%m/%y %H:%M:%S")
#a.untuk dapatin email username dan password pake read
read -p "email: " email

#Kurang tau unique itu apa tapi google biar ga duplikasi alamat email
if grep -q "$email" ./users/users.txt;
then
        echo "[$waktu] [REGISTER FAILED] $email already exists" >> ./users/auth.log
        echo "REGISTER FAILED - email $email already exists"
        exit 1
fi

read -p "username: " username
read -p "password: " password
echo

#b. buat passwordnya ribet
#pass harus lebih dari 8 karakter
if [[ ${#password} -le 7 ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - password length must >= 8"
        exit 1
fi

#pass harus ada minimal 1 kapital dan 1 kecil (! = nagasi no)
if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - must contain 1 uppercase and 1 lowercase"
        exit 1
fi

#pass tidak boleh sama dengan username
if [[ "$password" == "$username" ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - username and password cannot be the same"
        exit 1
fi

#pass harus memiliki 1 angka
if ! [[ "$password" =~ [0-9] ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - must contain 1 number"
        exit 1
fi

#pass harus memiliki 1 simbol
if ! [[ "$password" =~ [!-@] ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - must contain 1 symbol"
        exit 1
fi

#pass di encrypt menggunakan base 64 (ini harusnya paling akhir ga sih)
password_baru=$(echo -n "$password" | base64)

#c. untuk nyimpen datae
echo "$email $username $password_baru" >> ./users/users.txt

#d. untuk mengetahui apakah registrasi berhasil
echo "[$waktu] [REGISTER SUCCESS] user $username registered successfully" >> ./users/auth.log
echo "REGISTER SUCCESS - user $username"
```
pertama diminta untuk membuat registrasi email dan password, mendapatkan username dan password dengan read, untuk unique ketika user yang pernah ada diregist lagi maka sistem akan meneluarkan "REGISTER FAILED - email $email already exists"
![image](/uploads/5eae83a0633b19ad9e6e0bfd9fa77577/image.png)

kemudian soal ingin ada password yang kuat harus >= 8 huruf menggunakan 'ge 8', minimal 1 huruf besar dan 1 huruf kecil menggunakan 'a-z' lalu 'A-Z', password tidak boleh sama dengan username menggunakan '==', minimal 1 angka dengan 0-9', untuk minimal 1 simbol kami menggunakan urutan ascii dimana '!-@'. Untuk error yang dihadapi adalah kebingungan untuk menggunakan apa sebagai range simbolnya sehingga banyak percobaan yang gagal sebelum ketemu ide ascii, dan terakhir enkripsi menggunakan base64
![image](/uploads/6ec9f3b1c2c7655ab3c2a664f315412f/image.png)

untuk menyimpan dengan 'echo "$email $username $password_baru" >> ./users/users.txt' tujuannya untuk menyimpan email user dan password yang sudah terenkripsi dikirim dari "." sebagai current dir ke folder users lalu ke users.txt. Isi user.txt:
![image](/uploads/24bf55158d41057423c98b757101cb1f/image.png)

respon sendiri itu ada balasan jika berhasil login atau tidak. Contoh berhasil dan tidak berhasil:
![image](/uploads/06bdef9bd6814ea20b3f59e6a6cec2c2/image.png)

login.sh
```bash
#!/bin/bash
#waktu sekarang
waktu=$(date +"%d/%m/%y %H:%M:%S")
#e. melakukan login
read -p "email: " email
read -p "password: " password
echo

#chek email
if ! grep -q "$email" ./users/users.txt;
then
        echo "[$waktu] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> ./users/auth.log
        echo "LOGIN FAILED - Email $email not registered, please register first"
        exit 1
fi

#untuk cek password
password_baru=$(grep "$email" ./users/users.txt | awk '{print $3}')
password_lama=$(echo "$password_baru" | base64 --decode)

if [[ "$password" == "$password_lama" ]];
then
        username=$(grep "$email" ./users/users.txt | awk '{print $2}')
        echo "[$waktu] [LOGIN SUCCESS] User $username login successfully" >> ./users/auth.log
        echo "LOGIN SUCCESS - Welcome, $username"
else
        echo "[$waktu] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> ./users/auth.log
        echo "LOGIN FAILED - Wrong password"
fi
```
pada login.sh mirip seperti register menerima email dengan cara read bedanya dimana ada pemeriksaan dengan melakukan gerp untuk memastikan email tersebut ada atau tidak lalu awk unutk mengambil password lalu mengubah password ke bentuk normal dan melakukan perbandingan

jika login gagal maka akan keluar pesan password salah dan jika benar akan keluar welcome. Contoh benar dan salah:
![image](/uploads/f5b0706d21fda7a8eec93c80b78be51c/image.png)

sama seperti user.txt tadi hanya saja ini dimasukkan kedalam auth.log dimana menggunakan perintah 'echo "bla bla bla" >> ./users/auth.log, dikirim dari "." sebagai current dir ke folder users lalu ke auth.log. Isi auth.log:
![image](/uploads/cc935cd26ef4873674bf839281511a15/image.png)

NB: ada time stampnya juga cara mendapatkan time stamp dengan 'waktu=$(date +"%d/%m/%y %H:%M:%S")'

### Soal 3
genshin.sh
```bash
#!/bin/bash


#donlod
wget -O genshin --no-check-certificate -r 'https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2'


#unzip fileny
unzip "genshin" -d "genshin_folder"
cd genshin_folder
touch char_data.csv

#unzip foto genshin cari yae wangy (aku kecewa foto yae nya ga wangy)
unzip "genshin_character.zip" -d "genshin_char"
cd genshin_char

#decrypt namafile
for file in *; do
  if [ -f "$file" ]; then
    filename="${file%.*}"
    decoded_file=$(echo "$filename" | base64 -d)
    mv "$file" "$decoded_file"
  fi
done

#klompokin sesuai kampung dll aduh jadi kebanyakan tau lah
mkdir Mondstat Liyue Inazuma Sumeru Fontaine
regions=("Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine")
elements=("Pyro" "Hydro" "Electro" "Cryo" "Geo" "Anemo" "Dendro")
weapons=("Sword" "Claymore" "Polearm" "Catalyst" "Bow")
char_region=""
char_element=""
char_weapon=""
sword_usr=0
claymore_usr=0
polearm_usr=0
catalyst_usr=0
bow_usr=0

#cek csv buat cari char_data
for file in *; do
  if [ -f "$file" ]; then
    cd ../
    grep "$file" list_character.csv > char_data.csv
    #find region di char_data
    for region in "${regions[@]}"; do
	result=$(awk -v word="$region" '$0 ~ word {print}' char_data.csv)
	if [ -n "$result" ]; then
	  char_region="$region"
	  break
	fi
    done

    #find elemen di char_data
    for element in "${elements[@]}"; do
        result=$(awk -v word="$element" '$0 ~ word {print}' char_data.csv)
        if [ -n "$result" ]; then
          char_element="$element"
          break
        fi
    done

    #find weapon di char_data
    for weapon in "${weapons[@]}"; do
        result=$(awk -v word="$weapon" '$0 ~ word {print}' char_data.csv)
        if [ -n "$result" ]; then
          char_weapon="$weapon"
	  #sekalian ngitungin siapaaja yang make ygy
	  case "$char_weapon" in
	    "Sword")
		((sword_usr++))
	    ;;
	    "Claymore")
                ((claymore_usr++))
            ;;
	    "Polearm")
                ((polearm_usr++))
            ;;
	    "Catalyst")
                ((catalyst_usr++))
            ;;
	    "Bow")
                ((bow_usr++))
            ;;
	  esac
          break
        fi
    done

    #rename foto wangy sesuai format
    cd genshin_char
    mv "$file" ./"$char_region"
    cd "$char_region"
    mv "$file" "$file - $char_region - $char_element - $char_weapon"
    cd ../
  fi
done

#dump owo
cd ../ && rm list_character.csv char_data.csv genshin_character.zip
cd ../ && rm genshin

#disuru print huft
echo "Number of character(s) using Sword: $sword_usr"
echo "Number of character(s) using Claymore: $claymore_usr"
echo "Number of character(s) using Polearm: $polearm_usr"
echo "Number of character(s) using Catalyst: $catalyst_usr"
echo "Number of character(s) using Bow: $bow_usr"
```
Hal pertama yang dilakukan adalah membuat bash script ‘genshin.sh’
Kemudian dilakukan download dari link yang diberikan dengan ‘wget’ dan ‘-O genshin’ untuk membuat hasil download dalam satu file zip “genshin”. Dilanjutkan dengan melakukan unzip “genshin” dan unzip “genshin_character” untuk mendapatkan foto-foto karakter. 

![image](/uploads/8ccd072f51f15245f33629eab86a71f2/image.png)

Nama dari karakter-karakter yang didapat tersebut dalam kondisi ter-enkripsi dengan base64, maka dilakukan dekripsi dengan memasukkan hasil dekripsi ke variabel ‘decoded_file’ kemudian dilakukan rename nama ‘file’ menjadi isi dari variabel ‘decoded_file’

![image](/uploads/bc1c4d6f4a942738d4719c25f41d6018/image.png)

![image](/uploads/0b0cfdafa42631553b71a9b992e7ea87/image.png)

Setelah mendapat nama asli dari setiap karakter, dilakukan pengecekan terhadap file ‘list_character.csv’ untuk mendapat data diri karakter tersebut dan dimasukkan dalam file baru yaiu ‘char_data’. Untuk mendapat region, elemen, dan senjata karakter tersebut dilakukan pengecekan kembali pada file ‘char_data’ dengan menggunakan array sebagai data kemungkinan yang ada dan dibandingkan dengan isi ‘char_data’. Semua data pribadi karakter dimasukkan dalam variabel yaitu ‘char_region’, ‘char_element’, dan ‘char_weapon’. Kemudian karakter tersebut dipindahkan dalam folder sesuai regionnya dan direname mengikuti format Nama - Region - Elemen - Senjata.

![image](/uploads/3c8ad9309bf604a15b95a4d53bfc853c/image.png)

Ketika melakukan pengecekan senjata karakter dilakukan juga increment terhadap jumlah pengguna senjata dengan menggunakan ‘case’. Proses pengecekan dilakukan berulang dengan ‘for’ loop untuk semua foto karakter.

![image](/uploads/5b7dc30607678ec4460512e71ce9764c/image.png)

![image](/uploads/d51e68d2edb715ad38f3798f77bf5f5f/image.png)

Setelah selesai melakukan semua itu, dilakukan delete file ‘list_character.csv’, ‘genshin_character.zip’, dan ‘genshin’ serta menampilkan jumlah pengguna tiap senjata.

![image](/uploads/3d1ac06c01e3928143445389a720ec55/image.png)

find_me.sh
```bash
#!/bin/bash

#list region
regions=("Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine")

#keluar masuk folder region
for region in "${regions[@]}"; do
cd ./genshin_folder/genshin_char/"$region"

  #extract foto2 wangy
  for file in *; do
    waktu=$(date +"%Y%m%d%H%M%S")
    img_path="./${file}.jpeg"
    if [ -f "$file" ]; then
	steghide extract -sf "$file" -p ""
	name=$(awk -F' -' '{print$1}' <<< "$file")
	cat "$name".txt | base64 -d > ~/sisop/modul_1/soal_3/extracted.txt
    fi

    status="NOT FOUND"
    url=$(cat ~/sisop/modul_1/soal_3/extracted.txt)
    url_regex='https?://\S+'
    if [ $url=~$url_regex ]; then
	#donlod
	wget "$url"
	status="FOUND"
    fi

    echo "[$waktu] [$status] [$img_path]" >> ~/sisop/modul_1/soal_3/image.log

    if [ $status="FOUND" ]; then
    	break
    fi
    rm "$name".txt

    sleep 60
  done
if [ $status="FOUND" ]; then
    	break
fi
cd ../../../
done

#pindain file eter lumin wangy ke depan + txt mami lisa
mv ~/sisop/modul_1/soal_3/genshin_folder/genshin_char/Mondstat/eab0659ba060b0624df5d28b4910b2fb.jpg ~/sisop/modul_1/soal_3/eab0659ba060b0624df5d28b4910b2fb.jpg
mv ~/sisop/modul_1/soal_3/genshin_folder/genshin_char/Mondstat/Lisa.txt ~/sisop/modul_1/soal_3/Lisa.txt
rm ~/sisop/modul_1/soal_3/extracted.txt
```
Untuk mencari clue endgame yang belum ditemukan, saya membuatkan satu script lagi yaitu ‘find_me.sh’
Demi mencari rahasia yang disembunyikan oleh Doktor Jenius, dilakukan extract pada setiap foto karakter dengan fungsi steghide dan didapat file (nama_karakter).txt yang berisi string namun dalam enkripsi base64. Kemudian dilakukan lagi dekripsi dengan base64 dan hasil dekripsi dimasukkan dalam file ‘extracted.txt’.
Isi dari ‘extracted.txt’ dipanggil dalam variabel ‘url’ untuk dilakukan pengecekan apakah merupakan sebuah url.

![image](/uploads/ea6e44e44733fa9d1b02aa397f9d8e3c/image.png)

Jika hasil ekstraksi tersebut tidak mengandung url maka akan dilakukan delet file ‘(nama_karakter).txt’ tersebut dan dilakukan pencatatan pada ‘image.log’ dengan format [waktu] [status] [path_gambar].

![image](/uploads/4229060f0ea0c2b62404251f201af68d/image.png)

Jika hasil ekstraksi mengandung url maka akan dilakukan download dari url tersebut dan pengecekkan akan dihentikan.

![image](/uploads/782288138e0086c3b3eec6ff439405bc/image.png)

Ekstraksi gambar dilakukan dengan selang satu menit menggunakan ‘sleep 60’ didalam ‘for’ loop untuk memberi jeda 60 detik sebelum dilanjutkan ke gambar berikutnya.

![image](/uploads/45137c554d0cd7900c36b4f4531d32e4/image.png)

Hasil akhirnya adalah sebagai berikut dengan file ‘.jpg’ sebagai rahasia yang disembunyikan Doktor Jenius.

![image](/uploads/3288308f3f8b3be21e19c84271937b40/image.png)

### Soal 4
minute_log.sh
```bash
#!/bin/bash
#crontab
#* * * * * ~/sisop/modul_1/soal_4/minute_log.sh
#membuat file.log
#Mendapatkan waktu
waktu=$(date +"%Y%m%d%H%M%S")

#Mendapatkan ram dengan menggunakan perintah free -m
#NR==2 untuk memori, NR==3 untuk swap
ram=$(free -m | awk 'NR==2{print $2","$3","$4","$5","$6","$7}')
swap=$(free -m | awk 'NR==3{print $2","$3","$4}')

#Menentukan direktori target
path=~/

#Mendapatkan disk dengan menggunakan perintah du -sh
disk=$(du -sh "${path}" | awk '{print $1}')

#Menyimpan ram, path, disk ke dalam file log
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/"sisop/modul_1/soal_4/log/metrics_${waktu}.log"
echo "${ram},${swap},${path},${disk}" >> ~/"sisop/modul_1/soal_4/log/metrics_${waktu}.log"
chmod 600 ~/"sisop/modul_1/soal_4/log/metrics_${waktu}.log"
```
pada minute_log.sh ada time stamp lalu megambil informasi ram, swap, path, dan disk lalu semua data itu disimpan kedalam './log/metrics_${waktu}.log', kenapa disimpan didalam folder log karena notes soal berkata seperti itu. Untuk isi dari log folder dan metrics seperti ini:

![image](/uploads/1bbeb9c30e5dea7f8ec39c664e783ccb/image.png)

untuk metrics diatas diharapkan dapat berjalan otomatis pada setiap menit maka menggunakan cron tapi karena pada note disuruh cukup command aja maka cukup '#* * * * * /home/steven/sisop/modul_1/soal_4/minute_log.sh'

aggregate_minutes_to_hourly_log.sh
```bash
#!/bin/bash
#crontab
#0 * * * * /home/steven/sisop/modul_1/soal_4/aggregate_minutes_to_hourly_log.sh
#mendapatkan waktu saat ini
waktu=$(date +"%Y%m%d%H")
#declarasi nilai awal
count=0
path=~/
mkdir ~/sisop/modul_1/soal_4/log/combined_log/

#menghitung semua file log per menit yang sesuai dengan format waktu ke dalam satu file
for file in ~/sisop/modul_1/soal_4/log/*.log; do
    hour="${file:17:2}"
    cat "$file" >> ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log"
done
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
# Calculate max
max_memtot=$(awk -F',' '{ if ($1 > max_memtot || NR == 1) max_memtot = $1 } END { print max_memtot }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_memus=$(awk -F',' '{ if ($2 > max_memus || NR == 1) max_memus = $2 } END { print max_memus }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_memfr=$(awk -F',' '{ if ($3 > max_memfr || NR == 1) max_memfr = $3 } END { print max_memfr }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_memsh=$(awk -F',' '{ if ($4 > max_memsh || NR == 1) max_memsh = $4 } END { print max_memsh }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_membf=$(awk -F',' '{ if ($5 > max_membf || NR == 1) max_membf = $5 } END { print max_membf }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_memav=$(awk -F',' '{ if ($6 > max_memav || NR == 1) max_memav = $6 } END { print max_memav }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_swaptot=$(awk -F',' '{ if ($7 > max_swaptot || NR == 1) max_swaptot = $7 } END { print max_swaptot }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_swapus=$(awk -F',' '{ if ($8 > max_swapus || NR == 1) max_swapus = $8 } END { print max_swapus }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_swapfr=$(awk -F',' '{ if ($9 > max_swapfr || NR == 1) max_swapfr = $9 } END { print max_swapfr }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")

#get all pathsz and separate alphabet-numeric
pathsz=$(awk -F',' '{print $NF}' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
max_pathsz=$(echo "$pathsz" | sed 's/\([0-9]\+\)\([A-Za-z]\+\)/\1 \2/' | sort -nr | head -n 1 | awk '{print $1}')

# Calculate min
min_memtot=$(awk -F',' '{ if ($1 < min_memtot || NR == 1) min_memtot = $1 } END { print min_memtot }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_memus=$(awk -F',' '{ if ($2 < min_memus || NR == 1) min_memus = $2 } END { print min_memus }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_memfr=$(awk -F',' '{ if ($3 < min_memfr || NR == 1) min_memfr = $3 } END { print min_memfr }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_memsh=$(awk -F',' '{ if ($4 < min_memsh || NR == 1) min_memsh = $4 } END { print min_memsh }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_membf=$(awk -F',' '{ if ($5 < min_membf || NR == 1) min_membf = $5 } END { print min_membf }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_memav=$(awk -F',' '{ if ($6 < min_memav || NR == 1) min_memav = $6 } END { print min_memav }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_swaptot=$(awk -F',' '{ if ($7 < min_swaptot || NR == 1) min_swaptot = $7 } END { print min_swaptot }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_swapus=$(awk -F',' '{ if ($8 < min_swapus || NR == 1) min_swapus = $8 } END { print min_swapus }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_swapfr=$(awk -F',' '{ if ($9 < min_swapfr || NR == 1) min_swapfr = $9 } END { print min_swapfr }' ~/"sisop/modul_1/soal_4/log/combined_log/combined_metrics_${hour}.log")
min_pathsz=$(echo "$pathsz" | sed 's/\([0-9]\+\)\([A-Za-z]\+\)/\1 \2/' | sort -n | head -n 1 | awk '{print $1}')

#AVG
avg_memtot=$(((max_memtot + min_memtot) / 2))
avg_memus=$(((max_memus + min_memus) / 2))
avg_memfr=$(((max_memfr + min_memfr) / 2))
avg_memsh=$(((max_memsh + min_memsh) / 2))
avg_membf=$(((max_membf + min_membf) / 2))
avg_memav=$(((max_memav + min_memav) / 2))
avg_swaptot=$(((max_swaptot + min_swaptot) / 2))
avg_swapus=$(((max_swapus + min_swapus) / 2))
avg_swapfr=$(((max_swapfr + min_swapfr) / 2))
avg_pathsz=$(((max_pathsz + min_pathsz) / 2))

#hasil
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
echo "minimum,$min_memtot,$min_memus,$min_memfr,$min_memsh,$min_membf,$min_memav,$min_swaptot,$min_swapus,$min_swapfr,$path,$min_pathsz""M" >> ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
echo "maximum,$max_memtot,$max_memus,$max_memfr,$max_memsh,$max_membf,$max_memav,$max_swaptot,$max_swapus,$max_swapfr,$path,$max_pathsz""M" >> ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
echo "average,$avg_memtot,$avg_memus,$avg_memfr,$avg_memsh,$avg_membf,$avg_memav,$avg_swaptot,$avg_swapus,$avg_swapfr,$path,$avg_pathsz""M" >> ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"

rm -r ~/"sisop/modul_1/soal_4/log/combined_log/"

chmod 600 ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
```

untuk mendapat aggregate dari log file dalam satu jam terakhir, maka dicari log file yang memiliki kesamaan jam dan dimasukan dalam satu file log gabungan sementara.
Dari log gabungan sementara tersebut akan dilakukan pencarian nilai maksimum dan minimum untuk semua jenis data dengan 'awk' lalu dimasukan dalam variabel masing-masing.

Khusus untuk path size karena dalam string terdapat 2 jenis data yaitu integer dan char maka perlu dilakukan pemisahan.
![image](/uploads/38630b2686b7aecef589238abe2f1459/image.png)
Pemisahan dilakukan dengan 'sed' lalu ketika sudah mendapat hanya integer, dapat dibandingkan dengan path size lainnya untuk mendapat nilai maximum, minimum, dan average.

Dalam proses mendapatkan nilai maximum dan minimum digunakan 'awk' untuk melakukan read pada satu kolom di log gabungan sementara dan membandingkan satu persatu dengan nilai yang ada dalam variabel, jika lebih besar (maximum) atau lebih kecil (minimum) maka nilai dalam variabel akan direplace. 
Nilai average didapat dari hasil bagi jumlah nilai maximum dan minimum.

Setelah mendapat semua nilai yang dibutuhkan dalam variabel, dilakukan print out ke file log gabungan yang menampilkan nilai minimum, maximum, dan average seperti gambar dibawah.

![image](/uploads/6095b8a505dec97ffd9d1619cdd60822/image.png)

# Revisi

terdapat kekurangan tanda petik saat pengumpulan soal nomor 4 pada code minute_log.sh dan aggregatre_minutes_to_hourly_log.sh
kesalahan terletak pada baris paling akhir kedua file itu dimana harusnya ada petik contoh : chmod 600 ~/"sisop/modul_1/soal_4/log/metrics_agg_${waktu}.log"
![image](/uploads/8ef65932094ecb35839eebbf1cd19409/image.png)