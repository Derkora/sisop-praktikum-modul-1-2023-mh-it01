#!/bin/bash
#waktu sekarang
waktu=$(date +"%d/%m/%y %H:%M:%S")
#a.untuk dapatin email username dan password pake read
read -p "email: " email

#Kurang tau unique itu apa tapi google biar ga duplikasi alamat email
if grep -q "$email" ./users/users.txt;
then
        echo "[$waktu] [REGISTER FAILED] $email already exists" >> ./users/auth.log
        echo "REGISTER FAILED - email $email already exists"
        exit 1
fi

read -p "username: " username
read -p "password: " password
echo

#b. buat passwordnya ribet
#pass harus lebih dari 8 karakter
if [[ ${#password} -le 7 ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - password length must >= 8"
        exit 1
fi

#pass harus ada minimal 1 kapital dan 1 kecil (! = nagasi no)
if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - must contain 1 uppercase and 1 lowercase"
        exit 1
fi

#pass tidak boleh sama dengan username
if [[ "$password" == "$username" ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - username and password cannot be the same"
        exit 1
fi

#pass harus memiliki 1 angka
if ! [[ "$password" =~ [0-9] ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - must contain 1 number"
        exit 1
fi

#pass harus memiliki 1 simbol
if ! [[ "$password" =~ [!-@] ]];
then
        echo "[$waktu] [REGISTER FAILED] Invalid password format" >> ./users/auth.log
        echo "REGISTER FAILED - must contain 1 symbol"
        exit 1
fi

#pass di encrypt menggunakan base 64 (ini harusnya paling akhir ga sih)
password_baru=$(echo -n "$password" | base64)

#c. untuk nyimpen datae
echo "$email $username $password_baru" >> ./users/users.txt

#d. untuk mengetahui apakah registrasi berhasil
echo "[$waktu] [REGISTER SUCCESS] user $username registered successfully" >> ./users/auth.log
echo "REGISTER SUCCESS - user $username"
