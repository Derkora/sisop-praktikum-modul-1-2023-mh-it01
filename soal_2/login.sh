#!/bin/bash
#waktu sekarang
waktu=$(date +"%d/%m/%y %H:%M:%S")
#e. melakukan login
read -p "email: " email
read -p "password: " password
echo

#chek email
if ! grep -q "$email" ./users/users.txt;
then
        echo "[$waktu] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> ./users/auth.log
        echo "LOGIN FAILED - Email $email not registered, please register first"
        exit 1
fi

#untuk cek password
password_baru=$(grep "$email" ./users/users.txt | awk '{print $3}')
password_lama=$(echo "$password_baru" | base64 --decode)

if [[ "$password" == "$password_lama" ]];
then
        username=$(grep "$email" ./users/users.txt | awk '{print $2}')
        echo "[$waktu] [LOGIN SUCCESS] User $username login successfully" >> ./users/auth.log
        echo "LOGIN SUCCESS - Welcome, $username"
else
        echo "[$waktu] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> ./users/auth.log
        echo "LOGIN FAILED - Wrong password"
fi
